package ua.nure.garashchenko;

public class Demo {
	public static void main(String[] args) {
		var container = new Container();
		container.add(new Teacher("Яна Самвелловна"))
				.add(new Student("Дарья"))
				.add(new Student("Елизавета"))
				.add(new Student("Полина"))
				.add(new Student("Александра"))
				.add(new Student("Максим"))
				.add(new Teacher("Дмитрий Николаевич"))
				.add(new Teacher("Олександр Витальевич"))
				.add(new Teacher("Станислав Георгиевич"))
				.add(new Teacher("Евгения Ивановна"));
		container.print();		
	}
}
