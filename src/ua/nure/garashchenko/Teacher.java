package ua.nure.garashchenko;

public class Teacher extends Person {

	public Teacher(String name) {
		super(name);
	}

	@Override
	public String getName() {
		return super.name + ". ПРЕПОДАВАТЕЛЬ.";
	}

}
