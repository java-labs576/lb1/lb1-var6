package ua.nure.garashchenko;

public class Student extends Person {
	
	public Student(String name) {
		super(name);
	}

	@Override
	public String getName() {
		return super.name + ". СТУДЕНТ.";
	}
}
