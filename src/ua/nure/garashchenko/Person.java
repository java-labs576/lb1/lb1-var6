package ua.nure.garashchenko;

public abstract class Person implements Comparable<Person> {
	protected String name;
	
	public Person(String name) {
		this.name = name;
	}

	public abstract String getName();	
	
	@Override
	public int compareTo(Person o) {
		return this.name.compareTo(o.name);
	}
}
