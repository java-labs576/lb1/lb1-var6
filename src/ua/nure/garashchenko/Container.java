package ua.nure.garashchenko;

import java.util.SortedSet;
import java.util.TreeSet;

public class Container {
	
	private SortedSet<Person> set = new TreeSet<Person>();
	
	public Container add(Person person) {
		set.add(person);
		return this;
	}
	
	public void print() {
		for (Person person : set) {
			System.out.println(person.getName());
		}
	}
}
